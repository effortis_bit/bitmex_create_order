<?php
namespace controllers;

use models\Bitmex;

class MainController extends Controller{

    public function actionIndex(){

        if( isset($_POST['make_order']) ){

            $order = new Bitmex();

            try{

                $response = $order->makeOrder( $_POST['direction'], $_POST['type'], $_POST['symbol'], $_POST['amount'], $_POST['price'] );

                $this->render("form", [
                    "success" => print_r($response, true)
                ]);

            } catch ( \Exception $e ){

                $this->render("form", [
                    "error" => $e->getMessage()
                ]);

            }


        } else
            $this->render("form");

    }

}