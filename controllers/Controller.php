<?php
namespace controllers;

abstract class Controller{

    public static function run(){

        // Fetch controller and action
        $controller = isset($_GET['c']) ? 'controllers\\' . ucfirst( strtolower( $_GET['c'] ) ) . 'Controller' : 'controllers\\MainController';
        $action = isset($_GET['a']) ? 'action' . ucfirst( strtolower( $_GET['a'] ) ) : 'actionIndex';

        $controllerModel = new $controller();
        $controllerModel->$action();

    }

    protected function render($template, $params = []){

        extract( $params );

        require _BASE_FOLDER_ . DIRECTORY_SEPARATOR . "views" . DIRECTORY_SEPARATOR . "layout.php";

    }

}