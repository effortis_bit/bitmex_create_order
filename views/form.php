<div class="container">

    <p>&nbsp;</p>

    <div class="row">

        <div class="col-sm-6 col-sm-offset-3">

            <h3>Make Order</h3>

            <form method="post" action="">

                <div class="form-group">
                    <label for="direction">Direction of order:</label>
                    <select class="form-control" name="direction" id="direction" required>
                        <option value="buy">Buy</option>
                        <option value="sell">Sell</option>
                    </select>
                </div>

                <div class="form-group">
                    <label for="type">Order type:</label>
                    <select class="form-control" name="type" id="type" required>
                        <option value="market">Market</option>
                        <option value="limit">Limit</option>
                    </select>
                </div>

                <div class="form-group">
                    <label for="symbol">Symbol:</label>
                    <input type="text" class="form-control" name="symbol" id="symbol" value="" placeholder="String literal symbol of the market. Ex.: BTC/USD, ZEC/ETH, DOGE/DASH, etc..." required>
                </div>

                <div class="form-group">
                    <label for="amount">Amount:</label>
                    <input type="text" class="form-control" name="amount" id="amount" value="" placeholder="How much of currency you want to trade" required>
                </div>

                <div class="form-group">
                    <label for="price">Price:</label>
                    <input type="text" class="form-control" name="price" id="price" value="" placeholder="How much of currency you want to trade" required>
                </div>

                <div class="form-group">
                    <input type="submit" name="make_order" value="Submit order" class="btn btn-primary">
                </div>

            </form>

            <?php
            if( isset($error) && $error ){
                ?>
                <div class="alert alert-danger">
                    <?= $error ?>
                </div>
                <?php
            }

            if( isset($success) && $success ){
                ?>
                <div class="alert alert-success">
                    <?= $success ?>
                </div>
                <?php
            }
            ?>

        </div>

    </div>

</div>