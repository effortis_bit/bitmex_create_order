<?php
// Include constants
require_once "constants.php";

// Include vendor autoloader
require_once 'vendor/autoload.php';

// Classes autoloader
function namespace_autoloader($class)
{
    $filename = _BASE_FOLDER_ . DIRECTORY_SEPARATOR . str_replace('\\', DIRECTORY_SEPARATOR, $class) . '.php';
    if( file_exists($filename) )
        require_once($filename);
}
spl_autoload_register('namespace_autoloader');

\controllers\Controller::run();