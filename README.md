# Bitmex order creation

Creating a market/limit order on Bitmex.
You can provide the type of the order, direction (sell/buy), amount and the price for some currency symbol.

### Installation

Clone the repo
```
git clone https://<username>:<password>@bitbucket.org/effortis_bit/bitmex_create_order.git .
```

Run composer update for installing CCXT library
```
composer update
```

That's it!