<?php
namespace models;

class Bitmex{

    private $connection;

    public function __construct(){

        $this->connection = new \ccxt\bitmex ([
            "apiKey" => _BITMEX_API_KEY,
            "secret" => _BITMEX_SECRET,
            "urls" => [
                "api" => _BITMEX_API_URL
            ]
        ]);

    }

    public function makeOrder( $side, $type, $symbol, $amount, $price ){

        return $this->connection->create_order($symbol, $type, $side, $amount, $price);

    }

}